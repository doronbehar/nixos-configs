{ pkgs, ... }:

{
  config = {
    environment.systemPackages = import ./pkgsLists/shell.nix pkgs;
    # Also configured similarly in programs.neovim module. See
    # https://github.com/NixOS/nixpkgs/pull/330083
    environment.pathsToLink = [ "/share/nvim" ];

    nix.settings = {
      experimental-features = "nix-command flakes";
    };
    programs.bandwhich.enable = true;

    programs.less.enable = true;
    programs.zsh.enable = true;
    # See https://github.com/NixOS/nix/pull/4132
    programs.zsh.enableCompletion = false;
    # Use nix-index' better command-not-found
    programs.command-not-found.enable = false;
    programs.nix-index = {
      enable = true;
      enableFishIntegration = false; # Don't use it
    };
    environment.extraSetup =
      # Install multiple pinentry programs properly
    ''
      ln -s ${pkgs.pinentry-curses}/bin/pinentry $out/bin/pinentry-curses
      ln -s ${pkgs.pinentry}/bin/pinentry $out/bin/pinentry-tty
    '' +
      # 'Install' neovim-unwrapped for help in debugging neovim/vim issues
    ''
      ln -s ${pkgs.neovim-unwrapped}/bin/nvim $out/bin/nvim-unwrapped
    ''
    ;
    security.sudo.extraConfig = ''
      Defaults timestamp_type = global
    '';
  };
}
