{ ... }:

{
  nix.distributedBuilds = true;
  nix.buildMachines = [
    {
      system = "x86_64-linux";
      # This host is actually configured in /root/.ssh/config, along with the ssh key
      hostName = "office";
      supportedFeatures = [ "kvm" "big-parallel"];
    }
  ];
  nix.extraOptions = ''
    builders-use-substitutes = true
  '';
}

