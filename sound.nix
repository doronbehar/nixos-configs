{ pkgs, ... }:

{
  config = {
    # Add myself to this group
    users.users.doron.extraGroups = [ "audio" ];
    services.pulseaudio.enable = true;
    services.pulseaudio.package = pkgs.pulseaudioFull;
    # services.pulseaudio.extraModules = [
      # pkgs.pulseaudio-modules-bt
    # ];
    services.pulseaudio.extraConfig = ''
      .ifexists module-bluetooth-policy.so
      load-module module-bluetooth-policy
      .endif
      .ifexists module-bluetooth-discover.so
      load-module module-bluetooth-discover headset=ofono
      .endif
    '';
    services.pulseaudio.tcp.enable = true;
    services.pulseaudio.tcp.anonymousClients.allowedIpRanges = [
      "127.0.0.1"
      "192.168.1.0/24"
      "192.168.14.0/24"
      "192.168.43.0/24"
    ];
    environment.systemPackages = builtins.attrValues { inherit (pkgs)
      # cli mixer for pulseaudio
      pulsemixer
    ;};
  };
}
