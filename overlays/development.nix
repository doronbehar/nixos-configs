    self: super: {
      # Install all outputs of gnumake
      gnumake = super.gnumake.overrideAttrs (oldAttrs: {
        meta = oldAttrs.meta // {
          outputsToInstall = oldAttrs.outputs;
        };
      });
      # https://github.com/NixOS/nixpkgs/pull/385556
      google-clasp = super.google-clasp.overrideAttrs (oldAttrs: {
        postInstall = ''
         rm $out/lib/node_modules/@google/clasp/node_modules/.bin/sshpk-{verify,sign,conv}
        '';
      });
    }
