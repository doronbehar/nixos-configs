    self: super: {
      # wrapper arguments - *Used to be* shared for both beta and stable,
      # an unneeded abstraction, now with Nvim 0.5.0 released.
      neovim-wrapped-args = {
        # Python 3 packages I need for certain plugins
        extraPython3Packages = (p: [
          # for https://github.com/raghur/vim-ghost
          p.simple-websocket-server
          p.python-slugify
          # For https://github.com/peterbjorgensen/sved
          p.dbus-python
          p.pygobject3
        ]);
        # No python2, probably a deprecated argument
        #withPython = false;
        # Who the hell uses ruby plugins?
        withRuby = false;
        configure = {
          customRC = ''
            if $XDG_CONFIG_HOME ==# '''
	            let $XDG_CONFIG_HOME = $HOME . '/.config'
            endif
            source $XDG_CONFIG_HOME/nvim/init.vim
          '';
          packages.nixPlugins = {
            start = [
              (self.vimPlugins.nvim-treesitter.withPlugins (
                plugins: with plugins; [
                  tree-sitter-go
                  tree-sitter-c
                  tree-sitter-cpp
                  tree-sitter-nix
                  tree-sitter-javascript
                  tree-sitter-lua
                  tree-sitter-python
                  tree-sitter-r
                  tree-sitter-ruby
                  tree-sitter-rust
                  tree-sitter-sql
                  # TODO: Add to nixpkgs
                  #tree-sitter-vala
                  tree-sitter-vim
                  # config files formats
                  tree-sitter-toml
                  tree-sitter-yaml
                  tree-sitter-json
                  tree-sitter-json5
                  # typesetting 
                  tree-sitter-markdown
                  tree-sitter-bibtex
                  tree-sitter-latex
                  tree-sitter-html
                  # Basic, (not real) languages
                  tree-sitter-bash
                  tree-sitter-make
                  tree-sitter-cmake
                  # TODO: Add to nixpkgs
                  #tree-sitter-ninja
                ]
              ))
            ];
          };
        };
      };
      neovim = super.wrapNeovim self.neovim-unwrapped self.neovim-wrapped-args;
      # Vim configured
      vim_configurable = super.vim_configurable.override {
        # This overlay triggers a rebuild anyway, so we might as well use a
        # better lua distribution.
        lua = self.luajit;
        guiSupport = "false";
        rubySupport = false;
        multibyteSupport = true;
        ftNixSupport = false;
        cscopeSupport = false;
      };
      # Install all outputs of ncurses - for `toe` - install terminfo imperatively
      ncurses = super.ncurses.overrideAttrs (oldAttrs: {
        meta = oldAttrs.meta // {
          outputsToInstall = oldAttrs.outputs;
        };
      });
      # Unfortuneatly, ruby_3_2 breaks compilation of some packages asciidoctor-with-most-extensions needs
      rubyMinimal = super.ruby.override {
        jitSupport = false;
        yjitSupport = false;
      };
      gopass = super.gopass.override {
        xclip = null;
      };
      asciidoctor-with-most-extensions = super.asciidoctor-with-extensions.override {
        withJava = false;
        bundlerApp = super.bundlerApp.override {
          ruby = self.rubyMinimal;
        };
      };
    }
