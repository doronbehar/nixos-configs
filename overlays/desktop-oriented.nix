    self: super: {
      # Add mpris plugin to mpv
      mpv-with-scripts = super.wrapMpv self.mpv-unwrapped {
        scripts = [super.mpvScripts.mpris];
      };
      # Fix annoying mutter upstream issue
      gnome = super.gnome.overrideScope (pself: psuper: {
        mutter = psuper.mutter.overrideAttrs (oldAttrs: {
          # From some reason, unique is required, as the patch is applied twice from some reason.
          patches = (oldAttrs.patches or [ ]) ++ [
            # manual revert of: https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2878
            ./mutter-annoying-fucus.patch
          ];
        });
      });
      # Revive feedreader, see https://github.com/NixOS/nixpkgs/pull/171384
      feedreader-revived = self.stdenv.mkDerivation rec {
        pname = "feedreader";
        version = "2.11.0";
        src = self.fetchFromGitHub {
          owner = "jangernert";
          repo = pname;
          rev = "v${version}";
          sha256 = "1agy1nkpkdsy2kbrrc8nrwphj5n86rikjjvwkr8klbf88mzl6civ";
        };
        nativeBuildInputs = [
          self.meson
          self.ninja
          self.pkg-config
          self.vala
          self.gettext
          self.appstream-glib
          self.desktop-file-utils
          self.libxml2
          self.python3
          self.wrapGAppsHook
        ];
        buildInputs = [
          self.curl
          self.glib
          self.json-glib
          self.libnotify
          self.libsecret
          self.sqlite
          self.gumbo
          self.gtk3
          self.libgee
          self.libpeas
          self.gnome.libsoup
          self.librest
          self.webkitgtk
          self.gsettings-desktop-schemas
          self.gnome-online-accounts
        ] ++ (with self.gst_all_1; [
          gstreamer
          gst-plugins-base
          gst-plugins-good
        ]);
        postPatch = ''
          patchShebangs build-aux/meson_post_install.py
        '';
        meta = with self.lib; {
          description = "A modern desktop application designed to complement existing web-based RSS accounts";
          homepage = "https://jangernert.github.io/FeedReader/";
          license = licenses.gpl3Plus;
          maintainers = with maintainers; [ doronbehar ];
          platforms = platforms.linux;
        };
      };
      # Add plugins to urxvt
      rxvt-unicode = super.rxvt-unicode.override {
        configure = { availablePlugins, ... }: {
          plugins = [
            availablePlugins.bidi
            availablePlugins.perl
            availablePlugins.perls
            availablePlugins.font-size
          ];
        };
      };
      # Configured build of mlterm
      mlterm-my = super.mlterm.override {
        enableX11 = true;
        enableGuis = {
          xlib = false;
          fb = false;
          wayland = true;
          sdl2 = false;
        };
        enableTools = {
          # Interesting tool but I don't need it for my simple usage.
          mlclient = false;
          # Buggy, and I already used it to configure mlterm and saved the dotfiles.
          mlconfig = false;
          # Already configured font by hand.
          mlfc = false;
          # No idea what are these 2:
          mlimgloader = false;
          registobmp = false;
        };
        enableFeatures = {
          uim = false;
          ibus = false;
          fcitx = false;
          m17n = false;
          skk = false;
          # I don't know what is this feature
          ssh2 = false;
          bidi = true;
          # Open Type layout support, (substituting glyphs with opentype fonts)
          otl = true;
        };
      };
      mathematica = super.mathematica.override {
        version = "14.1.0";
        webdoc = true;
      };
      libreoffice = super.libreoffice-fresh.override {
        dbusVerify = false;
        extraMakeWrapperArgs = [
          # Use the light, not dark theme.
          "--set-default" "GTK_THEME" "Adwaita"
        ];
      };
      davmail = super.davmail.override {
        preferZulu = false;
      };
      gnomeExtensionScreencast = super.gnomeExtensions.easyScreenCast.overrideAttrs(old: new: {
        version = "1.11.0";
        # Use a patch, so that in the next nixos-rebuild this patch will fail
        # to apply and hence require me to remove it.
        patches = [
          (super.fetchpatch {
            url = "https://github.com/EasyScreenCast/EasyScreenCast/compare/1.10.0...1.11.0.patch";
            hash = "sha256-SGVBzE9XaYp9apKIsJdjzkQOZrOb3Z58eTJFBPhazo4=";
          })
        ];
      });
    }
