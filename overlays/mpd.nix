self: super: {
  beets = (super.beets-unstable.override {
    # select plugins in beets
    pluginOverrides = {
      mbsubmit = { enable = true; };
      acoustid = { enable = true; };
      badfiles = { enable = false; };
      convert = { enable = false; };
      discogs = { enable = false; };
      embyupdate = { enable = false; };
      fetchart = { enable = true; };
      keyfinder = { enable = true; };
      kodiupdate = { enable = false; };
      limit = { enable = false; };
      substitute = { enable = false; };
      advancedrewrite = { enable = false; };
      autobpm = { enable = false; };
      lastfm = { enable = true; };
      mpd = { enable = true; };
      replaygain = { enable = true; };
      sonosupdate = { enable = false; };
      thumbnails = { enable = false; };
    };
  }).overridePythonAttrs(old: {
    patches = old.patches ++ [
      # Fix for https://github.com/NixOS/nixpkgs/issues/385389
      (super.fetchpatch {
        url = "https://github.com/beetbox/beets/pull/5650/commits/5c1817c780eb8886d359c73ddd79206adb42412e.patch";
        hash = "sha256-koCYeiUhk1ifo6CptOSu3p7Nz0FFUeiuArTknM/tpVQ=";
        excludes = [
          "docs/changelog.rst"
        ];
      })
    ];
  });
}
