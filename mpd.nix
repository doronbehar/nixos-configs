{ config, pkgs, lib, ... }:

{
  config = {
    services.mpdscribble = {
      enable = true;
      endpoints = {
        "libre.fm" = {
          username = "doronbehar";
          passwordFile = "/var/lib/secrets/libre-fm.pwd";
        };
        "last.fm" = {
          username = "doron-behar";
          passwordFile = "/var/lib/secrets/last-fm.pwd";
        };
      };
      passwordFile = "/var/lib/secrets/mpd";
    };

    services.mpd = {
      enable = true;
      group = "syncthing";
      credentials = [{
        permissions = ["read" "add" "control" "admin"];
        passwordFile = "/var/lib/secrets/mpd";
      }];
      network.listenAddress = "0.0.0.0";
      extraConfig = ''
        audio_output {
          type "pulse"
          name "Local Music Player Daemon"
          server "127.0.0.1"
        }
      '';
    };

    services.syncthing.settings.folders = {
      "/var/lib/mpd/playlists" = {
        label = "Music Player Daemon - Playlists";
        id = "mpdplaylists";
        type = "sendonly";
        devices = [ "Android" ];
      };
    };
    systemd.tmpfiles.rules = lib.lists.flatten (builtins.map (
      dir:
      [
        "d ${dir} 2775 mpd syncthing"
        "a ${dir}/ - - - - group:syncthing:rwx,other::rx"
        "a ${dir}/ - - - - default:group:syncthing:rwx,default:other::rx"
      ]
    )
      (
        [ "/var/lib/mpd/other" "/var/lib/mpd/music" ] ++
        (builtins.attrNames config.services.syncthing.settings.folders)
      )
    ) ++ [
      "d /var/lib/mpd 2755 mpd syncthing"
    ];
    # By default the mpd service treats the mpd/music directory as a state
    # directory, and hence changes the permissions of it to 0755. I want these
    # permissions to be 0775, as done in the above tmpfiles rules. The
    # following overrides what the service does.
    systemd.services.mpd.serviceConfig.StateDirectory = lib.mkForce [
      "mpd"
    ];

    environment.systemPackages = import ./pkgsLists/mpd.nix pkgs;
  };
}
