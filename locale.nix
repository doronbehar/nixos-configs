{ config, lib, ... }:

{
  config = {
    # Select internationalisation properties.
    i18n = lib.optionalAttrs (config.networking.hostName != "vps") {
      defaultLocale = "en_IL.UTF-8";
      supportedLocales = [
        "en_IL/UTF-8"
        "he_IL.UTF-8/UTF-8"
      ];
    };
    # Makes switching to Hebrew etc possible on the console
    console.useXkbConfig = true;
    # The font that supports Hebrew
    console.font = "iso08.16";

    # also sets time.timeZone = null;
    services.tzupdate.enable = true;
    # TODO: Check if this should be part of Nixpkgs' service:
    systemd.services.tzupdate = {
      wantedBy = [ "multi-user.target" ];
    };
    systemd.timers.tzupdate = {
      timerConfig = {
        OnStartupSec = "30s";
        Persistent = true;
      };
      wantedBy = [ "timers.target" ];
    };
  };
}
