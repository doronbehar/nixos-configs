# Needed for configuring syncthing devices - comes from configuration.nix
# calling this module with `(import ./<file>.nix syncthing-private)` and
# configuration.nix gets it from flake.nix
syncthing-private:

{ pkgs, ... }:

{
  config = {
    # Syncthing
    services.syncthing = {
      enable = true;
      # Allow folders and devices to be set imperatively
      overrideFolders = true;
      overrideDevices = true;
      settings = {
        inherit (syncthing-private)
          devices
          folders
        ;
      };
      openDefaultPorts = true;
    };
    # Since we don't use custom certificates, we can set this command to
    # whatever we want. Most of the directories are located in /home/doron, and
    # systemd by default removes x and r permission for the users directory.
    users.users.doron.homeMode = "711";
    # Put myself in this group
    users.users.doron.extraGroups = [ "syncthing" ];
    # tmpfiles are defined in ~/.config/user-tmpfiles.d/syncthing.conf
    environment.systemPackages = builtins.attrValues { inherit (pkgs)
      # To debug the syncthing-init.service API connecting script - for xmllint
      libxml2
    ;};
  };
}
