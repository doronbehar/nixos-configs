{ pkgs, ... }:

{
  config = {
    programs.firejail = {
      enable = true;
    };
    services.udev.extraRules = ''
      # For arduino
      SUBSYSTEM=="usb", ENV{ID_VENDOR_ID}=="1781", ENV{ID_MODEL_ID}=="0c9f", MODE="0660", GROUP="plugdev"
    '';
    programs.adb.enable = true;
    environment.systemPackages = import ./pkgsLists/development.nix pkgs;
    users.users.doron.extraGroups = [
      "docker"
      "adbusers"
    ];
    # Docker
    virtualisation.docker.enable = true;
  };
}
