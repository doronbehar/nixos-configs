pkgs: (builtins.attrValues {
  inherit (pkgs)
    # manage library
    beets
    # For beets-check - https://github.com/geigerzaehler/beets-check
    liboggz
    mp3val
    # to rip CDs and downloads
    sacd # Rips ISO files
    abcde # Rips from connected discs
    flac
    flacon
    fuseiso
    monkeysAudio
    unflac # Split audio tracks from Cue
    shntool
    cuetools
    wavpack
    # to contribute to musicbrainz
    picard
    # to-rip SACD (super audio cd) files
    # sacd # See https://github.com/NixOS/nixpkgs/pull/82692
    # basic cli interface
    mpc_cli
    # To manipulate video & audio
    ffmpeg-full
    # to view audio's data
    mediainfo
    # mpris client for mpd
    mpd-mpris
    # the best client
    ncmpcpp
    # To read a manual or to check version...
    mpd
  ;
})
