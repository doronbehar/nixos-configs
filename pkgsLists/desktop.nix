pkgs: (builtins.attrValues {
  inherit (pkgs)
    # For update-desktop-database command automatically run in ~/.local/share/applications
    desktop-file-utils
    # Desktop Environment
    evolution
    # Email notifications and indicator
    ayatana-webmail
    # musical tuner
    lingot
    # Replaces gnome-font-viewer
    font-manager
    # For editing music sheets
    musescore
    # The best calculator ever! Incldues scintific units conversion
    qalculate-gtk
    # For integrals etc, Wolfram Mathematica alternative, ideally I should be
    # using https://github.com/tweag/jupyterWith with:
    # https://github.com/robert-dodier/maxima-jupyter
    wxmaxima
    # Live LaTeX rendering
    texpresso
    # Wolfram mathematica - can't be used not on a desktop
    mathematica
    # Nothing can bit
    vlc
    # CLI and lua lgi library for accessing media players (mpris)
    playerctl
    # To pick colors
    gcolor3
    # to interact with the browser
    brotab
    # Web browser
    firefox
    ungoogled-chromium
    # web browser theme
    #shadowfox - currently broken nix package
    # radio without internet (SDR)
    gqrx
    fractal
    tdesktop
    # Handling pdfs and jpegs pngs
    poppler_utils
    pdf2svg
    pdfcpu
    pdfarranger
    pdfsam-basic
    # NOTE how it is overriden in ./overlays/desktop-oriented.nix
    libreoffice
    # Video cropper (cutter) - based on ffmpeg
    video-trimmer
    # images shrinks
    optipng
    jpegoptim
    # transmission indicator
    transmission-remote-gtk
    # PDF editing and annotating tool
    inkscape
    xournalpp
    # manage clipboard from cli
    wl-clipboard
    # managing "desktop" passwords from cli - works only when x running,
    # meant for systemd user units
    libsecret
    # graphical disk partioning tool
    gparted
    # This will always stay my faveorite
    alacritty
    # Interesting alternative, indeed works great with bidi writings
    mlterm-my
    # Get QR code from the camera (`zbarcam`)
    zbar
    # tikz images creator
    tikzit
    # notifications from cli
    libnotify
    # xdotool for wayland
    ydotool
    # Download books from libgen
    libgen-cli
    # Syncthing tray application
    syncthingtray-minimal
    # Some people need my excel abilities
    # Mircrosoft exchange <-> {imap & smtp} bridge, used only on desktop
    # because I configure it via a GUI, and it has also a tray application
    davmail
    # Movies subtitles related programs
    #python3.pkgs.subliminal - currently broken
    subberthehut
    # Gnome related
    gnome-screenshot
    gnome-tweaks
    gnome-maps
    gnome-weather
    gnome-power-manager
    gnome-sound-recorder
    nautilus
    # for crafting shell scripts with GUI elements
    zenity
    gnomeExtensionScreencast # From overlay..
  ;

  # Sensors, hardisk, cpu temperature etc
  gnomeExtensionVitals = pkgs.gnomeExtensions.vitals;
  #gnomeExtensionScreencast = pkgs.gnomeExtensions.easyScreenCast;
  inherit (pkgs) lm_sensors;
  inherit (pkgs.kdePackages) breeze-icons;
})
