pkgs: (builtins.attrValues {
  inherit (pkgs)
    # to view tor network stats
    nyx
    # To debug networking issues
    iw
    # too basic to comment
    curlFull
    wget
    # word diff like git diff --word-diff
    wdiff
    tmux
    gitFull
    git-lfs
    lf
    pistol # my Baby
    tree
    file
    htop
    pciutils
    ncurses # For `toe`
    inetutils # For `telnet`
    psmisc
    lsof
    lz4
    ansifilter
    fzf
    rsync
    # for entering failed nix builds via `breakpointHook`. See:
    # https://discourse.nixos.org/t/debugging-broken-builds/3138/3?u=doronbehar
    cntr
    # for improving diffs in git
    diff-so-fancy
    # for editorconfig support in neovim & vim
    editorconfig-core-c
    # github client
    gh
    # gitlab client
    glab
    jq # json parser
    gjo # json cli generator
    pup # css selectors html parser
    url-parser # gets parts of urls
    yq-go # YAML processor
    initool # INI files processor (useful for systemd services debugging)
    vdirsyncer khard # For mutt's query_command
    # to debug elfs
    strace
    unzip
    zip
    # cat with wings
    bat
    # Text editor
    neovim
    neovim-remote
    # to record and share in the web shell sessions
    asciinema
    # for crafting shell commands with sml parsing handling
    xmlstarlet
    # for crafting shell commands with date handling
    dateutils
    # for automatically pretty printing multi-directional text
    fribidi bicon
    # push notifications client
    gotify-cli
    # for "watching" files and folders in crafted scripts / commands
    entr
    # to inspect network speed
    speedtest-cli
    # to inspect network hosts
    whois
    # to find nix commands easily
    nix-index
    # To diff nix systems
    nix-diff
    # To find problematic nix drvs in store
    nix-tree
    # nix version diff tool
    nvd
    # mount from ssh
    sshfs-fuse
    # ssh utility
    socat
    # markup convertor
    pandoc
    # Manage DNS records etc
    #gandi-cli  # Disabled temporarily due to python312 issues
    # To test gmailctl configs
    jsonnet
    # Often I need this - to spawn a transmission-daemon for fast downloads
    transmission_4
    nmap
    # Reverse tethering
    gnirehtet
    goimapnotify
    msmtp
    isync
    neomutt
    urlscan
    # manage gmail filters from the CLI
    gmailctl
    # awesome asciidoc implementation
    asciidoctor-with-most-extensions
    # wake up network hosts
    wol
    gopass-jsonapi
    # youtube downloads
    yt-dlp
    # google translate on the CLI
    translate-shell
    # tasks
    taskwarrior3
    taskwarrior-tui
    # Android stuff
    android-file-transfer
    heimdall
    # For deleting merged remote branches automatically
    git-trim
    # Very useful at visualizing git branches ancestary
    git-big-picture
    # Usually I use neovim, but only sometimes good old Vim is needed
    vim_configurable
    # Sometimes needed, usually for mutt
    w3m-nox
    antiword
    catdoc
    xlsx2csv
    # Naturally this would have resided in ./development.nix, but only here
    # the overlay defines it
    rubyMinimal
    # I have 1 perl script in my private dotfiles
    perl
    # Best syncer in town
    rclone
    # I don't use programs.gnupg.enable, but I mange it my self in
    # ~/.config/systemd/user
    gnupg
    # https://github.com/maralorn/nix-output-monitor
    nix-output-monitor
    # For https://github.com/Mic92/nixpkgs-review#review-changes-inside-sandbox-experimental
    bubblewrap
    # For web development
    live-server
    gopass
  ;
  # Passwords
  nvtop = pkgs.nvtopPackages.full; # top for gpu usage
})
