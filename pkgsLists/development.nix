# Now using `with pkgs;`, see: https://nix.dev/guides/best-practices#with-scopes
pkgs: (builtins.attrValues {
  inherit (pkgs)
    # for infocmp
    gawkInteractive
    # gnumake - for info docs
    gnumake
    # for arduino development
    avrdude
    arduino-cli
    # for nix dev
    cachix
    hydra-check
    # To browse store paths according to gc roots
    nix-tree
    # To browse nixos options in a curses interface like a pro
    nix-inspect
    nix-update
    nixpkgs-review
    nixfmt-rfc-style
    # language servers
    nixd # Nix
    delve # debugger actually
    gopls
    texlab # LaTeX
    svls # verilog
    vhdl-ls
    svlint
    # Usually used within a context of a flake.nix, but useful to put here
    # for the sake of the vim files of it
    lammps
    cmake-language-server
    arduino-language-server
    dockerfile-language-server-nodejs
    matlab-language-server
    vim-language-server
    autotools-language-server
    bash-language-server
    yaml-language-server
    vscode-langservers-extracted
    perlnavigator
    # for inspecting dbus stuff
    d-spy
    # You know what this is
    gdb
    # for image processing
    imagej
    gwyddion
    # needed for png / svg export of plots from octave
    ghostscriptX
    openjdk
    go
    gotools # for godoc mostly
    balena-cli
    tectonic-unwrapped # latex compiler
    pplatex # parses log files of tectonic, see: https://github.com/lervag/vimtex/issues/2703
    biber-for-tectonic # bibliography
    python3
    cmake # sometimes I need to edit such files and that's needed for cmake LSP
    # linux dev man pages
    man-pages
    # to get nix tarballs shas beforehand, and more
    nix-prefetch nix-init
    # development environment
    direnv
    # For inspecting binaries' "strings"
    binutils-unwrapped
    # For clangd and friends
    clang-tools
    # For firefox addons development
    web-ext
    # For google app scripts development
    google-clasp
  ;
  # Useful for small python scripts without external deps, in real world
  # projects, I add python packages to my PATH via a flake.nix
  inherit (pkgs.python3.pkgs) jedi-language-server;
})
