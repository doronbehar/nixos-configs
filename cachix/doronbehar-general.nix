
{
  nix = {
    settings = {
      substituters = [
        "https://doronbehar-general.cachix.org"
      ];
      trusted-public-keys = [
        "doronbehar-general.cachix.org-1:Z02FrdKyRXp0Hp/hHkHCW1A4dvC8O3cIMvuLTCCsHtg="
      ];
    };
  };
}
