
{
  nix = {
    settings = {
      substituters = [
        "https://robotnix.cachix.org"
      ];
      trusted-public-keys = [
        "robotnix.cachix.org-1:+y88eX6KTvkJyernp1knbpttlaLTboVp4vq/b24BIv0="
      ];
    };
  };
}
