{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    syncthing-private.url = "gitlab:doronbehar/nixos-syncthing-directories";
    nix-xilinx = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "gitlab:doronbehar/nix-xilinx";
    };
    # A nice tool not in Nixpkgs yet
    nix-inspect = {
      url = "github:bluskript/nix-inspect";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Available in Nixpkgs, but I apply some patches to it from:
    # https://github.com/nix-community/nix-index/pulls
    nix-index = {
      # Using branch myfork, so that branch master will still track
      # origin/master, in case I'll ever want to contribute patches upstream,
      # and upstream will be responsive enough to merge patches
      url = "github:doronbehar/nix-index/myfork";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-wsl = {
      url = "github:nix-community/NixOS-WSL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self
    , nixpkgs
    , nix-xilinx
    , nix-inspect
    , nix-index
    , syncthing-private
    , home-manager
    , nixos-wsl
  }:
  let
    overlays = {
      "x86_64-linux" = [
        nix-xilinx.overlay
        (self: super: {
          nix-inspect = nix-inspect.packages.x86_64-linux.default;
        })
        (self: super: {
          nix-index = nix-index.packages.x86_64-linux.default;
        })
        (import ./overlays/mpd.nix)
        (import ./overlays/development.nix)
        (import ./overlays/desktop-oriented.nix)
        (import ./overlays/shell-programs.nix)
      ];
    };
    # If at some point in life I'll want to use Nix on a machine without root
    # priviledges, this configuration might be useful:
    homeConfigurations = {
      office = home-manager.lib.homeManagerConfiguration {
        pkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = overlays.x86_64-linux;
        };
        modules = [
          ./home-manager-office.nix
          (import ./nixpkgs.nix nixpkgs overlays.x86_64-linux)
        ];
      };
    };
    commonModules = [
      ./locale.nix
      ./shell-programs.nix
      # Development oriented settings
      ./cachix.nix
      ./development.nix
      ./myuser.nix
      (import ./nixpkgs.nix nixpkgs overlays.x86_64-linux)
    ];
  in {
    inherit homeConfigurations;
    nixosConfigurations = {
      ZENIX = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = commonModules ++ [
          ({ ... }: { networking.hostName = "ZENIX"; })
          ./mpd.nix
          # Samba and nfs
          ./shares.nix
          # Hardware related
          ./hardware.nix
          ./sound.nix
          ./printers-scanners.nix
          ./radio.nix
          ./desktop-oriented.nix
          (import ./syncthing.nix syncthing-private)
          ./tor.nix
          # Sets up office as remote builder
          #./office-builder.nix # Will not happen on this period of life...
          ./avahi.nix
        ];
      };
      office = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = commonModules ++ [
          ({ ... }: { networking.hostName = "office"; })
          ./ssh-host.nix
        ] ++ nixos-wsl.nixosModules.wsl.imports ++ [
          ({ ... }: {
            config = {
              hardware.graphics = {
                enable = true;
              };
              services.xserver.videoDrivers = [
                "nvidia"
              ];
              hardware.nvidia = {
                # Segfaults, see:
                # https://github.com/NixOS/nixpkgs/issues/171361
                nvidiaSettings = false;
                open = true;
              };
              wsl = {
                enable = true;
                wslConf.automount.root = "/mnt";
                defaultUser = "doron";
                startMenuLaunchers = true;
              };
              system.stateVersion = "22.05";
            };
          })
        ];
      };
    };
  };
}
