{ pkgs, ... }:

{
  config = {
    environment.systemPackages = [
      pkgs.tmate
    ];
    # I'm not really using these, unfortunately. I use tmate instead - I
    # couldn't find a reasonable way to ensure a connection between two
    # machines behind heavy firewalls / NAT.
    #services.openssh.enable = true;
    #services.avahi.extraServiceFiles = {
    #  ssh = "${pkgs.avahi}/etc/avahi/services/ssh.service";
    #};
  };
}
