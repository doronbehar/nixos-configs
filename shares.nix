{ ... }:

{
  config = {
    services.samba = {
      enable = true;
      settings = {
        Music = {
          browseable = "yes";
          "read only" = true;
          "guest ok" = "yes";
          path = "/var/lib/mpd/music";
          comment = "All Music as indexed by beets";
        };
      };
      nsswins = true;
      openFirewall = true;
    };
    # Web Services Dynamic Discovery
    services.samba-wsdd = {
      enable = true;
      discovery = true;
    };
    networking.firewall.allowedTCPPorts = [ 5357 ];
    networking.firewall.allowedUDPPorts = [ 3702 ];
    services.nfs.server = {
      enable = true;
      exports = ''
        "/var/lib/mpd/music" *(ro,async,no_subtree_check)
      '';
    };
    services.avahi.extraServiceFiles = {
      smb = ''
        <?xml version="1.0" standalone='no'?><!--*-nxml-*-->
        <!DOCTYPE service-group SYSTEM "avahi-service.dtd">
        <service-group>
          <name replace-wildcards="yes">%h</name>
          <service>
            <type>_smb._tcp</type>
            <port>445</port>
          </service>
        </service-group>
      '';
    };
  };
}

