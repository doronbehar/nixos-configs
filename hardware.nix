{ lib, pkgs, ... }:

let
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/4b9e8e6b-bdee-4525-aa99-c91511c962ae";
      fsType = "btrfs";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/911C-E0FB";
      fsType = "vfat";
    };
  };
in {
  config = {
    boot.initrd.availableKernelModules = [
      "xhci_pci"
      "nvme"
      "usb_storage"
      "sd_mod"
    ];
    boot.kernelModules = [
      "kvm-intel"
    ];
    boot.extraModulePackages = [
    ];

    inherit fileSystems;
    swapDevices = [
      { device = "/dev/disk/by-uuid/b2b9a098-090d-483a-97a8-b7c13c777553"; }
    ];

    powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

    services.btrfs.autoScrub = {
      enable = true;
      # All of them
      fileSystems = builtins.attrNames (
        lib.filterAttrs (
          n: v:
          v.fsType == "btrfs"
        ) fileSystems
      );
    };

    # Ideally this should be true, but:
    # https://github.com/NixOS/nix/issues/7154
    boot.tmp.useTmpfs = false;
    # Use a tmpdir with unlimited space (as opposed to /tmp)
    systemd.services."nix-daemon".environment.TMPDIR = "/nix/tmp";
    systemd.tmpfiles.rules = [
      "d /nix/tmp 770 root nixbld" # Don't use it
    ];

    # Use the systemd-boot EFI boot loader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;

    # Add hardware related packages
    environment.systemPackages = builtins.attrValues { inherit (pkgs)
      # I may need to handle exfat drives
      exfat
      # inspect usb stuff
      usbutils
      # for god damn fat32 & ntfs formatted drives
      mtools ntfs3g
    ;};

    boot.kernelPackages = pkgs.linuxPackages;

    services.fstrim.enable = true;
    services.fstrim.interval = "monthly";
    hardware.bluetooth = {
      enable = true;
      powerOnBoot = false;
      package = pkgs.bluez;
      settings = {
        General = {
          Privacy = "off";
        };
      };
    };

    hardware.cpu.intel.updateMicrocode = true;

    hardware.firmware = builtins.attrValues { inherit (pkgs)
      firmwareLinuxNonfree
    ;};
  };
}
