{ ... }:

{
  config = {
    hardware.bladeRF.enable = true;
    hardware.rtl-sdr.enable = true;
    hardware.hackrf.enable = true;
    users.users.doron.extraGroups = [
      "bladerf"
      "plugdev"
    ];
  };
}
