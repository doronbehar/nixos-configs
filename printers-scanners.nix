{ pkgs, ... }:

let
  # Ports required to be open for hp-setup to find printers (udp and tcp)
  hpPorts = [
    # According to: https://developers.hp.com/hp-linux-imaging-and-printing/KnowledgeBase/Troubleshooting/TroubleshootNetwork
    161 162 9100
  ];
in {
  config = {
    # Enable CUPS to print documents. Note issue summarised here:
    # https://github.com/OpenPrinting/cups/issues/176#issuecomment-1845137340
    services.printing = {
      enable = true;
      drivers = [ pkgs.hplip ];
      openFirewall = true;
    };
    networking = {
      # The modern firewall
      nftables.enable = true;
      firewall = {
        allowedTCPPorts = hpPorts;
        allowedUDPPorts = hpPorts;
        allowedUDPPortRanges = [
          # Apparantly hplip needs this. TODO: Report this upstream? Or talk
          # about it on Nix' forums? See also:
          # https://discourse.nixos.org/t/how-to-detect-what-firewall-ports-i-need-to-open-for-a-certain-service/37314/4
          { from = 49152; to = 65535; }
        ];
      };
    };
    # scanners
    services.saned.enable = true;
    hardware.sane = {
      enable = true;
      extraBackends = [
        pkgs.hplip
        pkgs.sane-airscan
      ];
      openFirewall = true;
    };
    users.users.doron.extraGroups = [ "scanner" "lp" ];
    environment.systemPackages = builtins.attrValues { inherit (pkgs)
      # for hplip
      lsb-release
      # scan with gui
      simple-scan
    ;};
  };
}
