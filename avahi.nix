{ ... }:

{
  config = {
    services.avahi = {
      enable = true;
      nssmdns4 = true;
      nssmdns6 = true;
      reflector = true;
      openFirewall = true;
      publish = {
        enable = true;
        workstation = true;
        hinfo = true;
        userServices = true;
        domain = true;
      };
    };
  };
}
