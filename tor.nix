{ ... }:

{
  config = {
    services.tor = {
      enable = true;
      torsocks.enable = true;
      controlSocket.enable = true;
      settings.ControlPort = [{ port = 9051;}];
      client.enable = true;
      client.dns.enable = true;
    };
  };
}
