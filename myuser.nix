{ pkgs, ... }:

{
  config = {
    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.doron = {
      isNormalUser = true;
      shell = pkgs.zsh;
    };
    nix.settings.trusted-users = [
      "doron"
      # Apparently this is not trivial:
      "root"
    ];
    # Add myself to sudo users
    users.users.doron.extraGroups = [ "wheel" ];
  };
}
