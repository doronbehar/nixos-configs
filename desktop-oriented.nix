{ config, pkgs, lib, ... }:

{
  config = {

    # Xorg / gdm / wayland
    services.xserver.enable = true;
    services.xserver.displayManager.gdm = {
      enable = true;
      wayland = true;
    };
    # By default, in GDM's lock screen the power button does nothing. This
    # fixes this behavior to match the behavior of the desktop environment
    # https://wiki.archlinux.org/title/GDM#Configure_power_button_behavior
    programs.dconf.profiles.gdm.databases = [{
      settings."org/gnome/settings-daemon/plugins/power" = {
        power-button-action = "suspend";
        # 10-20 seconds until automatic suspend is enough
        sleep-inactive-ac-timeout = lib.gvariant.mkInt32 20;
        sleep-inactive-battery-timeout = lib.gvariant.mkInt32 10;
      };
    }];
    services.xserver.desktopManager.gnome.enable = true;
    services.xserver.xkb.layout = "us,il";
    # Enable touchpad support.
    services.libinput.enable = true;
    # services.xserver.xkbOptions = "grp:alt_shift_toggle";

    # gnome stuff
    services.pipewire = {
      # So that for sure I'd be able to record my screen..
      enable = true;
      # I still prefer using pulseaudio
      audio.enable = false;
      alsa.enable = false;
      pulse.enable = false;
    };
    services.gnome.evolution-data-server.enable = true;
    services.gnome.gnome-browser-connector.enable = true;
    services.gnome.gnome-remote-desktop.enable = false;
    services.gnome.core-os-services.enable = true;
    services.gnome.core-shell.enable = true;
    programs.evince.enable = true;
    programs.file-roller.enable = true;
    # programs.gnome-disks.enable = true;
    programs.seahorse.enable = true;
    services.gnome.sushi.enable = true;
    # Doesn't help if it's disabled via the excludePackages list, see
    # https://github.com/NixOS/nixpkgs/issues/24667
    programs.geary.enable = false;
    programs.gnome-terminal.enable = false;
    environment.gnome.excludePackages = builtins.attrValues {
      inherit (pkgs)
        gnome-console
        gnome-connections
        gnome-photos
        gedit
        epiphany
        gnome-calculator
        gnome-calendar
        totem
        yelp
        # I use font-manager instead
        gnome-font-viewer
        gnome-software
        gnome-music
        gnome-logs
      ;
    };
    programs.kdeconnect = {
      enable = true; # Mainly opens firewall ports 1714 to 1764
      package = pkgs.gnomeExtensions.gsconnect;
    };

    programs.nautilus-open-any-terminal = {
      enable = true;
      terminal = "alacritty";
    };

    # flatpak
    xdg.portal.enable = true;
    services.flatpak.enable = true;

    # Theming stuff
    gtk.iconCache.enable = true;
    # programs.qt5ct.enable = true;
    # qt5.enable = false;

    nixpkgs.config.packageOverrides = pkgs: {
      vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
    };
    hardware.graphics = {
      enable = true;
      extraPackages = builtins.attrValues { inherit (pkgs)
        vaapiIntel
        vaapiVdpau
        libvdpau-va-gl
        intel-media-driver
        intel-compute-runtime
      ;};
    };
    services.xserver.videoDrivers = [
      "modesetting"
      "nvidia"
    ];
    hardware.nvidia = {
      # https://bbs.archlinux.org/viewtopic.php?pid=2155426#p2155426
      package = config.boot.kernelPackages.nvidiaPackages.legacy_535;
      # Segfaults, see:
      # https://github.com/NixOS/nixpkgs/issues/171361
      nvidiaSettings = false;
      prime = {
        nvidiaBusId = "PCI:2:0:0";
        intelBusId = "PCI:0:2:0";
        offload = {
          enable = true;
          enableOffloadCmd = true;
        };
      };
    };

    programs.dconf.enable = true;

    services.gnome.gnome-keyring.enable = true;

    services.geoclue2.enable = true;

    environment.systemPackages = import ./pkgsLists/desktop.nix pkgs;

    environment.extraSetup = ''
      ln -s ${pkgs.pinentry-gnome3}/bin/pinentry $out/bin/pinentry-gnome3
    '' 
    # Fix for:
    # https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/1559#note_592512 
    # OR:
    # https://gitlab.gnome.org/GNOME/glib/-/blob/glib-2-64/gio/gdesktopappinfo.c#L2546
    + ''
      ln -sf $out/bin/{alacritty,rxvt}
    '';

    # Fonts
    fonts.enableDefaultPackages = true;
    fonts.fontDir.enable = true;
    fonts.packages = with pkgs; [
      # icon fonts
      material-design-icons
      nerd-fonts.hack
      # For scintific writings
      libertinus
      # for hebrew
      culmus
    ];
    # i2c for ZENIX and https://github.com/mohamed-badaoui/asus-touchpad-numpad-driver
    hardware.i2c.enable = true;
    systemd.services.asus-touchpad-numpad = {
      description = "Activate Numpad inside the touchpad with top right corner switch";
      documentation = ["https://github.com/mohamed-badaoui/asus-touchpad-numpad-driver"];
      path = [ pkgs.i2c-tools ];
      script = ''
        cd ${pkgs.fetchFromGitHub {
          owner = "mohamed-badaoui";
          repo = "asus-touchpad-numpad-driver";
          rev = "d80980af6ef776ee6acf42c193689f207caa7968";
          sha256 = "sha256-JM2wrHqJTqCIOhD/yvfbjLZEqdPRRbENv+N9uQHiipc=";
        }}
        ${pkgs.python3.withPackages(ps: [ ps.libevdev ])}/bin/python asus_touchpad.py ux433fa
      '';
      # Probably needed because it fails on boot seemingly because the driver
      # is not ready yet. Alternativly, you can use `sleep 3` or similar in the
      # `script`.
      serviceConfig = {
        RestartSec = "1s";
        Restart = "on-failure";
      };
      wantedBy = [ "multi-user.target" ];
    };

    # Make mouse not wake up machine when suspend - useful for desktop machines, that's why it's here
    systemd.services.disable-events-wakeup = {
      description = "Disable mouse and other events wake up";
      script = ''
        for code in GLAN XHC; do
          if grep -q "$code\s*S[0-9]\s\*enabled" /proc/acpi/wakeup; then
            echo "$code" > /proc/acpi/wakeup
          fi
        done
      '';
    };

    # Better keep this constant, for matlab installation or such ...
    networking.hostId = "5a812194";
    networking.networkmanager = {
      unmanaged = [
        "vmnet" "vboxnet" "virbr" "ifb" "ve" "docker0" "docker1" "docker2"
      ];
    };
  };
}
