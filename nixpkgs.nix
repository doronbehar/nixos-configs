nixpkgs:
overlays:

{ ... }:

{
  nix = {
    nixPath = [
      # Remove duplicate search results because of both nixpkgs and nixos channels used for the root and my user
      # See https://discourse.nixos.org/t/use-only-1-channel-for-my-users-nix-env-and-roots-nixos-rebuild/6311
      "nixpkgs=${nixpkgs.outPath}"
    ];
    registry = {
      # Add a nixpkgs-current flake to the local registry, that will
      # contain the nixpkgs that was used for evaluation the currently
      # running system. Useful in case of no network access, and to check
      # things against currently running system.
      nixpkgs-current = {
        to = {
          path = "${nixpkgs.outPath}";
          type = "path";
        };
      };
    };
  };
  nixpkgs = {
    inherit overlays;
    config = {
      # Opengl / video hardware acceleration, and for gpu accelarated
      # calculations
      allowUnfree = true;
      nvidia.acceptLicense = true;
    };
  };
  # I use it on the web
  documentation.nixos.enable = false;
}
